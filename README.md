#Accordion JS test

Cześć, przed Tobą bardzo prosty test znajomości jQuery.

Napisz prosty skrypt, który zapamięta ostatnio otwartą zakładkę akordeonu.
Informację o zakładce przechowuj w ciasteczku i przy następnym odświeżeniu strony wyświetl zapamiętaną zakładkę.

Jeśli użytkownik jest na naszej stronie po raz pierwszy - wyświetl pierwsza zakładkę z góry.

Do dyspozycji masz:

* Framework: Twitter Bootstrap
* jQuery-2.2.4
* jQuery.Cookie 1.4.1

**Proszę zwracać uwagę na dobre praktyki JavaScript.**